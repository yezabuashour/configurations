[[ ! -d ~/tmp ]] && mkdir ~/tmp
[[ ! -d ~/.cache ]] && mkdir ~/.cache
[[ ! -d ~/repo ]] && mkdir ~/repo

sudo dnf upgrade -y

sudo dnf reinstall shadow-utils -y

sudo dnf install \
wget \
vim \
git \
zsh \
zsh-syntax-highlighting \
nodejs \
passwd \
cracklib-dicts \
iproute \
findutils \
ncurses \
unzip -y

usermod -s /bin/zsh $USER
ln -s ~/repo/dotfiles/.zshrc ~/.zshrc