echo "making tmp & repo directories"
mkdir /home/elvishwook/tmp /home/elvishwook/repo
cd /home/evlishwook/tmp

echo "updating and upgrading"
sudo apt update && sudo apt upgrade -y

echo "installing packages"
sudo apt install \
curl unzip less groff \
postgresql-client \
git-all \
iputils-ping \
npm \
python3-pip \
gnupg \
software-properties-common \
curl \
zsh -y

echo "installing terraform"
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform

echo "installing aws cli"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

echo "updating node"
sudo npm cache clean -f
sudo npm install -g n
sudo n stable

echo "switching to zsh"
chsh -s $(which zsh)