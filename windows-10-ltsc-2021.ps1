Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All -n

Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

choco install docker-desktop -y
choco install sublimetext4 -y
choco install vscode -y
choco install stremio -y
choco install qbittorrent -y
choco install postman -y
choco install microsoft-windows-terminal -y
choco install spotify -y
choco install 7zip -y
choco install firefox -y
choco install googlechrome -y
choco install discord.install -y
choco install google-drive-file-stream -y
choco install microsoft-teams.install -y
choco install potplayer -y

# will need to do a restart -> wait for command to run the rest of the following:
# choco install wsl2 -y
# choco install wsl-ubuntu-2004 -y

# validate
# choco uninstall steam-client -y
# choco install lghub -y
# choco install leagueoflegends -y
# choco install battle.net -y
# choco install epicgameslauncher -y
# choco install goggalaxy -y
# choco install obs-studio -y

# research
# Rivatuner

# mkdir and ping to quick access
# wsl repo
# New-Item -Path "~\" -Name "tmp" -ItemType "directory"